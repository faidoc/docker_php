Ubuntu Docker container for PHP web applications
====================================================

Minimal base image for running PHP applications. Required 
packages are bundled into a single container, based on Ubuntu 14.04 server.

These services run with process supervision, using [Supervisor](http://supervisord.org):

- nginx
- php5-fpm (with php5-mcrypt, php5-mysqlnd, and php5-curl)
- mysql-server
- openssh-server
- cron
- beanstalkd
- artisan queue:listen (optional, see below)

Additionally, these packages are preinstalled:

- nano
- git
- php5-cli
- mysql-client
- composer
- curl
- python (*dependency for supervisord)


Running a container
-------------------

**1.** Build the container:

	docker build -t faidoc_php

**2.** Run the Docker image as a new Docker container:

	docker run -d \
	-p 80:80 -p 443:443 -p 3306:3306 \
	-v /home/youruser/app:/share \
	faidoc_php

Replace '/home/youruser/app' with the path to the PHP application's root directory in the host. This directory is a shared 
volume and so can be used to access the application files in either the host or the container.

Connecting to a container with SSH
----------------------------------

**Production use**

For production, replace the insecure private key with a true private key:

**1.** Get the container IP

	docker ps -a
	docker inspect container_name | grep IPA


**2.** Generate a new key and copy the public key to the container

	ssh-keygen -t rsa
	cat ~/.ssh/yourkey.pub | ssh -i /home/insecure_key root@<IP address> "cat > /root/.ssh/authorized_keys"

There should then be two new files in the */home* directory: i) production.key ii) production.key.pub

**3.** Copy *production.key.pub* to */root/.ssh/authorized_keys* in the container. Note this is an overwrite, not an append 
(so all previously valid keys, including *insecure_key* will be removed).

	cat /home/production.key.pub | ssh -i /home/insecure_key root@<IP address> "cat > /root/.ssh/authorized_keys"

**4.** Connect with SSH:

	ssh root@<IP address>

Process status
--------------

**supervisorctl** can be used to control the processes that are managed by supervisor.

*In the container*:

	supervisorctl


Queue:listen
------------

The queue listener (**php artisan queue:listen**) can be added as supervised process by uncommenting the lines in
**/etc/supervisord/queue.conf** (in the container).
